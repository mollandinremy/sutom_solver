from pytest import mark
from src.solver import Solver


@mark.parametrize(
    "entry, expected",
    [
        ("aaaaa", 1),
        ("abc", 3),
        ("aabc", 3),
        ("abbcc", 3),
        ("", 0),
        ("abcdefghijklmnopqrstuvwxyz", 26),
    ],
)
def test_word_entropy(entry, expected):
    assert Solver.word_entropy(entry) == expected
