from pytest import mark
from src.solver import Solver


@mark.parametrize(
    "entry, expected",
    [
        ("a###", [(0, "a")]),
        ("a#b#", [(0, "a"), (2, "b")]),
        ("a##b", [(0, "a"), (3, "b")]),
        ("####", []),
        ("", []),
    ],
)
def test_parse_input(entry, expected):
    assert Solver.get_char_pos(entry) == expected
