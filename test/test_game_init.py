from pytest import mark
from src.game import Game


@mark.parametrize(
    "entry, expected",
    [({"words": ["totototo", "dut dut"], "min": 8, "max": 8}, "totototo")],
)
def test_word(entry, expected):
    assert (
        Game.generate_random_word(entry["words"], entry["min"], entry["max"])
        == expected
    )


@mark.parametrize(
    "entry, expected",
    [
        ("camion", "######"),
        ("playmobil", "#########"),
        ("1", "#"),
    ],
)
def test_hint_wordle(entry, expected):
    game = Game(word=entry)
    assert game.display == expected

@mark.parametrize(
    "entry, expected",
    [
        ("camion", "c#####"),
        ("playmobil", "p########"),
        ("1", "1"),
    ],
)
def test_hint_sutom(entry, expected):
    game = Game(word=entry,sutom=True)
    assert game.display == expected


@mark.parametrize(
    "entry, expected",
    [
        (["camion", "camion"], "camion"),
        (["camion", "c"], "c#####"),
        (["camion", "ca"], "ca####", ),
        (["camion", "caca"], "ca####", ),
        (["camion", "caribo"], "ca#i#(o)"),
        (["camion", "coribo"], "c(o)#i##"),
        (["carbonara", "cacatoess"], "ca#(a)#(o)###"),
        (["carbonara", "cacaocaca"], "ca##o#a#a"),
        (["carbonara", "cacackaca"], "ca####a#a"  ),
        (["nurse", "retro"], "(r)(e)###" ),
        (["nurse", "amere"], "###(r)e"),
        (["nurse", "large"], "##r#e"),
        (["nurse", "roter"], "(r)##(e)#"),
        (["nurse", "curie"], "#ur#e"),
        (["nurse", "burne"], "#ur(n)e"),
        (["nurse", "nurse"], "nurse"),
        (["cogiter", "cuisson"], "c#(i)##(o)#"),
        (["cogiter", "coiffer"], "co(i)##er"),
        (["cogiter", "cogiter"], "cogiter"),

    ],
)
def test_compare(entry, expected):

    game = Game(word=entry[0])

    assert game.compare(entry[0], entry[1]) == expected
