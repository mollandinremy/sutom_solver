from pytest import mark
from src.solver import Solver, LETTER_WEIGHT


@mark.parametrize(
    "entry, expected",
    [
        ("aaaaa", LETTER_WEIGHT["a"] * 5),
        ("abc", LETTER_WEIGHT["a"] + LETTER_WEIGHT["b"] + LETTER_WEIGHT["c"]),
        (
            "aabc",
            LETTER_WEIGHT["a"]
            + LETTER_WEIGHT["a"]
            + LETTER_WEIGHT["b"]
            + LETTER_WEIGHT["c"],
        ),
        ("", 0),
    ],
)
def test_word_weight(entry, expected):
    assert Solver.word_weight(entry, LETTER_WEIGHT) == expected
