from pytest import mark
from src.solver import Solver


@mark.parametrize(
    "word, hints, misplaced, excluded, length, expected",
    [
        ("banane", [], [], set(), 6, True),
        ("banane", [(0, "b")], [], set(), 6, True),
        ("banane", [(0, "c")], [], set(), 6, False),
        ("banane", [(0, "b"), (1, "a")], [], set(), 6, True),
        ("banane", [(0, "b"), (1, "d")], [], set(), 6, False),
        ("banane", [(0, "b"), (1, "a")], [(2, "f")], set(), 6, False),
        ("banane", [(0, "b"), (1, "a")], [(3, "n")], set(), 6, True),
        ("banane", [(0, "b"), (1, "a")], [(3, "n")], set("a"), 6, False),
        ("banane", [(0, "b"), (1, "a")], [(3, "n")], set("z"), 6, True),
    ],
)
def test_word_filter(word, hints, misplaced, excluded, length, expected):
    solver = Solver()
    solver.hints = hints
    solver.misplaced = misplaced
    solver.excluded = excluded
    solver.state = "#" * length
    assert solver.word_filter(word) is expected
