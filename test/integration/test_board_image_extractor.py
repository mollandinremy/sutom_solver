import numpy as np
from src.config.folders import DATA_FOLDER


from src.image_extractor import BoardExtractor
from numpy.testing import assert_array_equal


class TestBoardExtractor:
    def setup_class(self):
        self.image_extractor = BoardExtractor(str(DATA_FOLDER / "test.png"))

    def test_extract_board_from_image(self):
        cropped_image = self.image_extractor.get_cropped_image()
        with open(DATA_FOLDER / "cropped_image_potager.npy", "rb") as file_test:
            expected_cropped_image = np.load(file_test)
        assert_array_equal(cropped_image, expected_cropped_image)
