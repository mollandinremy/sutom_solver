from pytest import mark
from src.solver import Solver


@mark.parametrize(
    "entry, expected",
    [
        ("bateau", 6),
    ],
)
def test_word_similarity_score_1(entry, expected):
    solver = Solver()
    solver.words = ["bateau"]
    assert solver.compute_word_similarity_score(entry) == expected


@mark.parametrize("entry, expected", [("bateau", 6), ("pastis", 3)])
def test_word_similarity_score_2(entry, expected):
    solver = Solver()
    solver.words = ["bateau", "prouts"]
    assert solver.compute_word_similarity_score(entry) == expected
