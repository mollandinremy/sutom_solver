from hypothesis import example, given
from hypothesis.extra.numpy import arrays
from hypothesis import strategies as st
import numpy as np


from src.image_extractor import BoardExtractor
from numpy.testing import assert_array_equal


class TestBoardExtractor:
    def setup_class(self):
        self.image_extractor = BoardExtractor("")

    def test_get_top_left_corner_from_corners_happy_path_return_3(self):
        corners = np.array([[2124, 801], [2128, 1406], [1423, 1410], [1419, 805]])
        top_left_corner = self.image_extractor.get_top_left_corner_from_corners(corners)
        assert top_left_corner == 3

    def test_get_bottom_right_corner_from_corners_happy_path_return_1(self):
        corners = np.array([[2124, 801], [2128, 1406], [1423, 1410], [1419, 805]])
        bottom_right_index = self.image_extractor.get_bottom_right_corner_from_corners(
            corners
        )
        assert bottom_right_index == 1

    def test_order_board_coordinates(self):
        corners = np.array([[2124, 801], [2128, 1406], [1423, 1410], [1419, 805]])
        expected_ordered_coordinates = np.array(
            [[1419, 805], [2124, 801], [2128, 1406], [1423, 1410]]
        )
        actual_ordoned_coordinates = self.image_extractor.order_board_coordinates(
            corners
        )
        assert_array_equal(actual_ordoned_coordinates, expected_ordered_coordinates)

    @given(
        arrays(
            shape=(4, 2),
            dtype=np.uint16,
            elements=st.integers(min_value=0, max_value=10000),
        )
    )
    def test_order_board_coordinates_pbt(self, corners):
        ordoned_coordinates = self.image_extractor.order_board_coordinates(corners)
        assert (
            ordoned_coordinates[0][0] + ordoned_coordinates[0][1]
            <= ordoned_coordinates[1][0] + ordoned_coordinates[1][1]
        )
        assert (
            ordoned_coordinates[2][0] + ordoned_coordinates[2][1]
            >= ordoned_coordinates[3][0] + ordoned_coordinates[3][1]
        )
