from typing import List, Dict, Tuple
from loguru import logger

WORDS_PATH = "../data/fr_full.txt"
LETTER_WEIGHT = {
    "e": 12.10,
    "a": 7.11,
    "i": 6.59,
    "s": 6.51,
    "n": 6.39,
    "r": 6.07,
    "t": 5.92,
    "o": 5.02,
    "l": 4.96,
    "u": 4.49,
    "d": 3.67,
    "c": 3.18,
    "m": 2.62,
    "p": 2.49,
    "g": 1.23,
    "b": 1.14,
    "v": 1.11,
    "h": 1.11,
    "f": 1.11,
    "q": 0.65,
    "y": 0.46,
    "x": 0.38,
    "j": 0.34,
    "k": 0.29,
    "w": 0.17,
    "z": 0.15,
}


class Solver:
    def __init__(self, words=None, excluded=None, hints=None, misplaced=None, state=None):
        self.words = [] if words is None else words
        self.excluded = set() if excluded is None else excluded
        self.hints = [] if hints is None else hints
        self.misplaced = [] if misplaced is None else misplaced
        self.state = state

    def load_words(self, path):
        with open(path, "r") as f:
            self.words = list(line.rstrip().lower() for line in f)

    @staticmethod
    def word_entropy(word: str) -> int:
        """Return the number of different character in the word

        Args:
            word: Word to compute

        Returns:
            Number of different characters
        """
        return len(set(word))

    @staticmethod
    def word_weight(word: str, letter_weight: Dict[str, int]) -> int:
        """Compute word weight based on each letter weight (french language)

        Args:
            word: Word to compute
            letter_weight: Letter weight map

        Returns:
            Word weight
        """
        return sum(letter_weight.get(ch, 0) for ch in word)

    @staticmethod
    def get_char_pos(entry: str) -> List[Tuple[int, str]]:
        """Get position of chars in given string. Unkown character should be represented by an "#"

        Args:
            entry: Input to parse

        Returns:
            List tuple for known character position
        """
        logger.debug(f"Parsing input: {entry}")
        return [(i, ch) for i, ch in enumerate(entry) if ch != "#"]

    def word_filter(self, word: str) -> bool:
        """Remove words with misplaced or missing characters. Also remove words with excluded characters

        Args:
            word: Word to test

        Returns:
            True if the word math the criteria, False otherwise
        """
        if len(word) != len(self.state):
            return False
        if any(char not in word for _, char in self.misplaced):
            return False
        if any(word[pos] == char for pos, char in self.misplaced):
            return False
        if any(word[pos] != char for pos, char in self.hints):
            return False
        if any(char in word for char in self.excluded):
            return False
        return True

    def next_word(self, letter_weigth: Dict[str, int]) -> str:
        """Select the next best word

        Args:
            letter_weigth: Letter weight

        Returns:
            Selected word
        """
        entropy = map(self.word_entropy, self.words)
        weigth = map(lambda word: self.word_weight(word, letter_weigth), self.words)
        list_zip = sorted(list(zip(entropy, weigth, self.words)), reverse=True)
        logger.debug(list_zip)
        if not list_zip:
            logger.info("No words found with these criteria")
            return ""
        return list_zip[0][2]

    def refresh_words(self):
        logger.info("Refreshing word list")
        self.words = list(filter(lambda word: self.word_filter(word), self.words))

    def solve(self):
        while len(self.words) > 1:
            self.state = input("Latest sutom state (# for unknown characters): ")
            self.hints = self.get_char_pos(self.state)
            logger.debug(f"Char pos: {self.hints}")

            self.misplaced += self.get_char_pos(input("New misplaced characters: "))
            self.excluded = self.excluded.union(set(input("New excluded characters: ")))
            logger.debug(f"Misplaced chars: {self.misplaced}")
            logger.debug(f"Excluded chars: {self.excluded}")

            self.refresh_words()
            new_word = self.next_word(LETTER_WEIGHT)
            logger.info(f"New word chosen: {new_word!s}")
            self.words.remove(new_word)
        logger.info(f"Last words state: {self.words}")

    def compute_word_similarity_score(self, word) -> int:
        score = 0
        for word_to_compare in self.words:
            score += sum(c1 == c2 for c1, c2 in zip(word, word_to_compare))

        return score


def safe_int(message: str) -> int:
    """Ask user input and cast it as int

    Args:
        message: Prompt message

    Returns:
        Loaded integer
    """
    try:
        return int(input(message))
    except ValueError:
        logger.warning("Number required !")
        return safe_int(message)


if __name__ == "__main__":
    solver = Solver()
    solver.load_words(WORDS_PATH)
    solver.solve()
