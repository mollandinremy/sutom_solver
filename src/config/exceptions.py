class NoContourFoundException(Exception):
    """
    Exception raised when no contour is found in the image.
    """

    def __init__(self, *args: object) -> None:
        super().__init__(*args)
