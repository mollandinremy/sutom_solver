import cv2
import numpy as np

from src.config.exceptions import NoContourFoundException


class BoardExtractor:
    def __init__(self, image_path: str) -> None:
        self.image_path = image_path

    def get_cropped_image(self) -> np.ndarray:
        """From the initial screen image, we want to get only the SUTOM board
        in order to extract texts and information from it.


        Returns
        -------
            the cropped image of the SUTOM board
        """

        only_threshold_image = self._convert_image_to_threshold_image()
        grid_contours = self._find_grid_contours(only_threshold_image)
        approximated_polygon = cv2.approxPolyDP(
            grid_contours, 0.01 * cv2.arcLength(grid_contours, True), True
        )
        corners = cv2.convexHull(approximated_polygon)
        corners = corners.reshape(4, 2)
        coordinates = self.order_board_coordinates(corners)

        bottom_x = coordinates[2][1]
        top_x = coordinates[0][1]
        left_y = coordinates[0][0]
        right_y = coordinates[2][0]

        image_cropped = self.image[top_x:bottom_x, left_y:right_y]
        return image_cropped

    def _convert_image_to_threshold_image(self) -> np.ndarray:
        """Convert the screen image to a an image that is only black and white
        and easier to analyse and extract the board

        Returns
        -------
         image with only threshold pixels
        """
        self.image = cv2.imread(self.image_path)
        gray_scale_image = cv2.cvtColor(self.image, cv2.COLOR_BGR2GRAY)
        blured_image = cv2.GaussianBlur(gray_scale_image, (5, 5), 0)
        thresh_image = cv2.adaptiveThreshold(blured_image, 255, 1, 1, 11, 2)
        return thresh_image

    def _find_grid_contours(self, only_threshold_image: np.ndarray) -> np.ndarray:
        """Find the contours of the grid of the board

        Parameters
        ----------
        only_threshold_image : image with only threshold pixels

        Returns
        -------
            the contours of the grid of the board

        Raises
        ------
        NoContourFoundException
            if no contour is found in the image
        """
        all_contours_in_image, _ = cv2.findContours(
            only_threshold_image, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE
        )
        max_area = 0
        biggest_contour = None
        for current_contour in all_contours_in_image:
            area = cv2.contourArea(current_contour)
            if area > max_area:
                max_area = area
                biggest_contour = current_contour

        if biggest_contour is None:
            raise NoContourFoundException(
                f"No Sutom board contour found in {self.image_path}"
            )
        return biggest_contour

    def order_board_coordinates(self, corners):
        """Order the board coordinates to crop the original image

        Parameters
        ----------
        corners : coordinates of each board corner

        Returns
        -------
            list of this type:
                [
                    [top_left_x, top_left_y], [top_right_x, top_right_y],
                    [bottom_right_x, bottom_right_y], [bottom_left_x, bottom_left_y]
                ]
        """

        ordoned_coordinates = np.zeros((4, 2), dtype="uint16")

        top_left_index = self.get_top_left_corner_from_corners(corners)
        ordoned_coordinates[0] = corners[top_left_index]

        corners = np.delete(corners, top_left_index, 0)
        bottom_right_index = self.get_bottom_right_corner_from_corners(corners)

        ordoned_coordinates[2] = corners[bottom_right_index]
        corners = np.delete(corners, bottom_right_index, 0)

        # get top right and bottom left
        if corners[0][0] > corners[1][0]:
            ordoned_coordinates[1] = corners[0]
            ordoned_coordinates[3] = corners[1]
        else:
            ordoned_coordinates[1] = corners[1]
            ordoned_coordinates[3] = corners[0]
        ordoned_coordinates = ordoned_coordinates.reshape(4, 2)
        return ordoned_coordinates

    @staticmethod
    def get_top_left_corner_from_corners(corners: np.ndarray) -> int:
        """The left corner is the one with the smallest x,y coordinate

        Parameters
        ----------
        corners : list of the 4 corners of the SUTOM board

        Returns
        -------
         coordinates of the left corner
        """
        max_sum = 100000
        left_corner_index = 0

        for index, corner in enumerate(corners):
            if corner[0] + corner[1] < max_sum:
                max_sum = corner[0] + corner[1]
                left_corner_index = index
        return left_corner_index

    @staticmethod
    def get_bottom_right_corner_from_corners(corners: np.ndarray) -> int:
        """The right corner is the one with the biggest x,y coordinate

        Parameters
        ----------
        corners : list of the 4 corners of the SUTOM board

        Returns
        -------
            coordinates of the right corner
        """
        max_sum = 0
        bottom_right_corner_index = 0
        for index, corner in enumerate(corners):
            if corner[0] + corner[1] > max_sum:
                max_sum = corner[0] + corner[1]
                bottom_right_corner_index = index
        return bottom_right_corner_index
