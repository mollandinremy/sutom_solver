import json
import os
from logzero import logger
from solver import Solver, LETTER_WEIGHT

WORDS_PATH = os.getenv("word_path")


def generate_response(data: dict, status_code: int = 500):
    logger.debug(f"Generating response {status_code!s} with data: {data!s}")
    return {
        "statusCode": status_code,
        "body": json.dumps(data)
    }


def lambda_handler(event, _):
    logger.debug(f"Event received: {event}")
    body = json.loads(event.get("body", {}))
    hints, misplaced, excluded = body.get("hints"), body.get("misplaced"), body.get("excluded")

    if any(field is None for field in (hints, misplaced, excluded)):
        return generate_response({"error": "Missing required field"}, status_code=400)

    solver = Solver(hints=hints, misplaced=misplaced, excluded=excluded)
    solver.load_words(WORDS_PATH)
    solver.refresh_words()

    next_word = solver.next_word(LETTER_WEIGHT)

    if not next_word:
        logger.warning("No word found")
        generate_response({"error": "No word possible"}, status_code=400)

    logger.info(f"Next word should be {next_word}")
    return generate_response({"next_word": next_word}, status_code=200)
