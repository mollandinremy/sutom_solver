import pyautogui
import cv2
import numpy as np
import time
import pytesseract
from typing import Dict

pytesseract.pytesseract.tesseract_cmd = r'C:\Program Files\Tesseract-OCR\tesseract.exe'


class Viewer:
    def __init__(self, monitor: Dict):
        self.monitor = monitor

    @staticmethod
    def detect_shape():
        time.sleep(3)
        screenshot = np.array(pyautogui.screenshot(region=(monitor["left"], monitor["top"], monitor["width"], monitor["height"])))
        gray = cv2.cvtColor(screenshot, cv2.COLOR_BGR2GRAY)
        rgb = cv2.cvtColor(screenshot, cv2.COLOR_BGR2RGB)

        results = pytesseract.image_to_data(gray, config='--psm 13')
        print(results)

        _, threshold = cv2.threshold(gray, 180, 255, cv2.THRESH_BINARY)
        contours, _ = cv2.findContours(threshold, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

        print(pytesseract.image_to_string(threshold))

        i = 0
        for contour in contours:
            if i == 0:
                i = 1
                continue

            approx = cv2.approxPolyDP(contour, 0.01 * cv2.arcLength(contour, True), True)

            if len(approx) == 4:
                print(contour)
                cv2.drawContours(screenshot, [contour], 0, (0, 0, 255), 2)

        cv2.imshow('shapes', screenshot)

        cv2.waitKey(0)
        cv2.destroyAllWindows()


if __name__ == '__main__':
    monitor = {'top': 470, 'left': 1030, 'width': 500, 'height': 500}
    viewer = Viewer(monitor)
    viewer.detect_shape()
