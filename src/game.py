import itertools
import logging
import random
from loguru import logger
from itertools import compress
from collections import Counter

WORDS_PATH = "../data/words.txt"
MIN_LENGTH = 6
MAX_LENGTH = 10
MAX_TRIES = 6


class Game:
    def __init__(self, word_list=None, word=None, sutom=False):
        self.all_words = word_list if word_list else None if word else self.load_words(WORDS_PATH)
        self.word = word if word else self.generate_random_word(self.all_words, MIN_LENGTH, MAX_LENGTH)
        self.display = self.word[0] + ("#" * (len(self.word) - 1)) if sutom else "#" * len(self.word)
        self.tries_left = 6
        self.current_guess = ""
        self.is_won = False
        self.is_lost = False

    @staticmethod
    def load_words(path=WORDS_PATH):
        """
        Returns:
            object: list of words
        """
        with open(path, "r") as f:
            words = list(line.rstrip().lower() for line in f)
        return words

    @staticmethod
    def generate_random_word(words, min_length=5, max_length=10) -> object:
        """
        Args:
            max_length: min length for word
            min_length: max length for word
            words: list of words
        Returns: a random word from the file
        """

        words = [word for word in words if (min_length <= len(word) <= max_length)]
        return random.choice(words)

    def compare(self, res, guess) -> object:
        """

        Args:
            res: the word to find
            guess: the user guess

        Returns: display in a motus like syntax

        """
        # correct letters as a mask
        correct = [res_letter == guess_letter for (res_letter, guess_letter) in itertools.zip_longest(guess, res)]
        # letters left to find as a mask
        is_incorrect_mask = [not elem for elem in correct]
        # dict with letters left to find as key and number of occurences as value
        words_left = Counter(list(compress(res, is_incorrect_mask)))
        # a letter is considered misplaced if in the word but not found already
        # AND we must consider if the letter is left to find multiple times
        misplaced = []
        for l in guess:
            misplaced.append(words_left[l] > 0)
            words_left[l] -= 1
        # display the final result by applying correct + misplaced masks to guess
        self.display = ''.join([a if b else "(" + str(a) + ")" if c else "#" for a, b, c in
                                itertools.zip_longest(guess, correct, misplaced)])
        return self.display

    def play_game(self, ):
        # the game ends if we win or lose
        while not self.is_won and not self.is_lost:
            # infinite loop until user provides valid word
            self.tries_left -= 1
            while True:
                try:
                    raw_guess = input("Guess: ")
                    if len(raw_guess) != len(self.word):
                        raise ValueError("word must be the same size")
                    elif raw_guess not in self.all_words:
                        raise ValueError("word must exist")
                    self.current_guess = raw_guess
                except ValueError as e:
                    logger.error("\n" + str(e))
                else:

                    break
            # when we guess the exact word it's won
            if self.current_guess == self.word:
                self.is_won = True
            # if we have tries left, we get a hint
            elif self.tries_left > 0:
                print("tries left : " + str(self.tries_left))
                self.compare(self.word, self.current_guess)
                print(self.display)
            # if we don't have any try left we lose
            else:
                self.is_lost = True
        if self.is_won:
            print("congratzz")
        elif self.is_lost:
            print("not congratz")
        else:
            logging.info("you shouldn't end up here wtf")


if __name__ == '__main__':
    game = Game()
    game.play_game()
