variable "prefix" {
  type        = string
  description = "Resources prefix"
}

variable "region" {
  type        = string
  description = "AWS Region"
}

variable "env" {
  type        = string
  description = "Environment name"
}

variable "application" {
  type        = string
  description = "Application name"
}

variable "package_name" {
  type        = string
  description = "Lambda package name"
  default     = "latest.zip"
}

variable "timeout" {
  type = number
  description = "Lambda timeout"
}

variable "memory" {
  type = number
  description = "Lambda memory allocated"
}

locals {
  common = "${var.prefix}-${var.env}-${var.application}"

  tags = {
    Environment = var.env
    Application = var.application
    Terraform   = true
  }
}