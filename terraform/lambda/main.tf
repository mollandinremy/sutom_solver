locals {
  lambda_name = "${local.common}-lambda"
  s3_name     = "${local.common}-sources-s3"
}

module "lambda_api" {
  source  = "terraform-aws-modules/lambda/aws"
  version = "~> 2.34"

  function_name = local.lambda_name
  description   = "Lambda for API Gateway to expose SUTOM Solver through a REST API"
  handler       = "api.lambda_handler"
  runtime       = "python3.8"
  timeout       = var.timeout
  memory_size   = var.memory

  create_package = false
  s3_existing_package = {
    bucket = local.s3_name
    key    = var.package_name
  }

  environment_variables = {
    word_path = "fr_full.txt"
  }

  tags = {
    Name = local.lambda_name
  }
}