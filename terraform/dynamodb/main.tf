locals {
  dynamodb_name = "${local.common}-dynamodb"
}

module "dynamodb_table" {
  source = "terraform-aws-modules/dynamodb-table/aws"

  name     = local.dynamodb_name
  hash_key = "hk"
  sort_key = "sk"

  billing_mode = "PAY_PER_REQUEST"

  attributes = [
    {
      name = "hk"
      type = "S"
    },
    {
      name = "sk"
      type = "S"
    }
  ]

  tags = {
    Name = local.dynamodb_name
  }
}