locals {
  lambda = "${local.common}-lambda"
}

data "aws_lambda_function" "solver" {
  function_name = local.lambda
}

data "aws_route53_zone" "domain" {
  name = var.domain
}
