variable "prefix" {
  type        = string
  description = "Resources prefix"
}

variable "region" {
  type        = string
  description = "AWS Region"
}

variable "env" {
  type        = string
  description = "Environment name"
}

variable "application" {
  type        = string
  description = "Application name"
}

variable "domain" {
  type        = string
  description = "Domain name"
}

variable "record" {
  type        = string
  description = "Record name"
}

locals {
  common = "${var.prefix}-${var.env}-${var.application}"

  tags = {
    Environment = var.env
    Application = var.application
    Terraform   = true
  }
}