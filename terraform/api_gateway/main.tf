locals {
  loggroup_name = "${local.common}-logs"
  apigw_name    = "${local.common}-apigw"

  fqdn = "${var.record}.${var.domain}"
}

resource "aws_cloudwatch_log_group" "logs" {
  name = local.loggroup_name
}

module "acm" {
  source  = "terraform-aws-modules/acm/aws"
  version = "~> 3.0"

  domain_name = var.domain
  zone_id     = data.aws_route53_zone.domain.zone_id

  subject_alternative_names = [local.fqdn]

  wait_for_validation = true

  tags = {
    Name = local.fqdn
  }
}

module "api_gateway" {
  source = "terraform-aws-modules/apigateway-v2/aws"

  name          = local.apigw_name
  description   = "Expose SUTOM Solver as a REST API"
  protocol_type = "HTTP"

  domain_name                 = local.fqdn
  domain_name_certificate_arn = module.acm.acm_certificate_arn

  # Access logs
  default_stage_access_log_destination_arn = aws_cloudwatch_log_group.logs.arn
  default_stage_access_log_format          = "$context.identity.sourceIp - - [$context.requestTime] \"$context.httpMethod $context.routeKey $context.protocol\" $context.status $context.responseLength $context.requestId $context.integrationErrorMessage"

  # Routes and integrations
  integrations = {
    "POST /solve" = {
      lambda_arn = data.aws_lambda_function.solver.arn
    }
  }

  tags = {
    Name = local.apigw_name
  }
}

resource "aws_route53_record" "api" {
  zone_id = data.aws_route53_zone.domain.zone_id
  name    = var.record
  type    = "A"

  alias {
    name                   = module.api_gateway.apigatewayv2_domain_name_configuration[0].target_domain_name
    zone_id                = module.api_gateway.apigatewayv2_domain_name_configuration[0].hosted_zone_id
    evaluate_target_health = false
  }
}

resource "aws_lambda_permission" "apigw" {
  statement_id  = "${local.common}-apigw-permission"
  action        = "lambda:InvokeFunction"
  function_name = data.aws_lambda_function.solver.function_name
  principal     = "apigateway.amazonaws.com"
}
