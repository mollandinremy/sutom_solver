locals {
  s3_name = "${local.common}-sources-s3"
}

module "s3_bucket" {
  source = "terraform-aws-modules/s3-bucket/aws"

  bucket = local.s3_name
  acl    = "private"

  versioning = {
    enabled = true
  }
}
